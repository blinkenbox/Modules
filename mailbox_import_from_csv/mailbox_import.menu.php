<?php
 

// Menu

if($app->auth->is_admin()) {

	$items = array();

	$items[] = array( 'title' 	=> 'Mailbox import from CSV',
			  'target' 	=> 'content',
			  'link'	=> 'tools/mailbox_import.php');


	$module['nav'][] = array(	'title'	=> 'Import',
					'open' 	=> 1,
					'items'	=> $items);

	unset($items);
}

?>
