<?php
/*
Copyright (c) 2015, Till Brehm, ISPConfig UG
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

require_once('../../lib/config.inc.php');
require_once('../../lib/app.inc.php');

//* Check permissions for module
$app->auth->check_module_permissions('admin');

//* This is only allowed for administrators
if(!$app->auth->is_admin()) die('only allowed for administrators.');

$app->uses('tpl');

$app->tpl->newTemplate('form.tpl.htm');
$app->tpl->setInclude('content_tpl', 'templates/mailbox_import.htm');
$msg = '';
$error = '';


if(isset($_FILES['file']['name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
	$lines = file($_FILES['file']['tmp_name']);
	unset($lines[0]);
	
	$email_list = array();
	$sys_groupid = intval($_POST['groupid']);
	//$server_id = 1;
	$sys_user = $app->db->queryOneRecord("SELECT * FROM sys_user WHERE default_group = $sys_groupid");
	$sys_userid = $sys_user['userid'];
	
	// get the mailserver of the client
	// It seems like "default_mailserver" isn't used anymore so we get the mailserver from "mail_servers"
	$tmp = $app->db->queryOneRecord('SELECT mail_servers FROM sys_group,client WHERE sys_group.client_id = client.client_id and sys_group.groupid = '.$sys_groupid);
	$server_id = intval($tmp['mail_servers']);
	unset($tmp);
	
	if($server_id == 0) die('Invalid server ID');
	
	foreach($lines as $line) {
		$parts = explode(';',$line);
		
		// login name; full name; mail address;password;mailbox quota
		// Please remind: if $login_name != $email it gets overwritten on any config change of the mailbox
		// as long as "System" > "Main Config" > "Mail" > "Allow custom login name" isn't set
		$login_name = $app->db->quote(trim($parts[0]));
		$name = $app->db->quote(trim($parts[1]));
		$email = strtolower(trim($parts[2]));
		$password = $app->db->quote(trim($parts[3]));
		$quota = intval($parts[4]);
		$email_parts = $parts = explode('@',$email);
		$user = $app->db->quote($email_parts[0]);
		$domain = $app->db->quote($email_parts[1]);
		
		// Check if email domain exists
		$domain_rec = $app->db->queryOneRecord("SELECT * FROM mail_domain WHERE domain = '$domain'");
		if($_POST['test'] != 1 && !is_array($domain_rec)) {
                        $insert_data = array(
				"sys_userid" => $sys_userid,
				"sys_groupid" => $sys_groupid,
				"sys_perm_user" => 'riud',
				"sys_perm_group" => 'riud',
				"sys_perm_other" => '',
				"server_id" => $server_id,
				"domain" => $domain,
				"active" => 'y',
			);
			$app->db->datalogInsert('mail_domain', $insert_data, 'domain_id');
		}
		
		// encrypt the password
		$password_enc = $app->auth->crypt_password(stripslashes($password));
		
		$rec = $app->db->queryOneRecord("SELECT count(mailuser_id) as number FROM mail_user WHERE email = '".$user.'@'.$domain."'");
		if($rec['number'] > 0) {
			$status = 'Duplicate';
		} else {
			$status = 'OK';
			if($_POST['test'] == 1) $status = 'Test: OK';
		}
		
		$tmp = array();
		$tmp['name'] = $name;
		$tmp['email'] = $user.'@'.$domain;
		$tmp['password'] = $password;
		$tmp['quota'] = $quota;
		$tmp['status'] = $status;
		
		$email_list[] = $tmp;

		if($_POST['test'] != 1 && $status != 'Duplicate') {
			
			$app->uses('getconf');
			$mail_config = $app->getconf->get_server_config($server_id,'mail');

			$maildir = str_replace("[domain]",$domain,$mail_config["maildir_path"]);
			$maildir = str_replace("[localpart]",strtolower($user),$maildir);
			$homedir = $mail_config["homedir_path"];
			$uid = $mail_config["mailuser_uid"];
			$gid = $mail_config["mailuser_gid"];
			$quota_bytes = $quota*1024*1024;
			
			// We create a new record
                        $insert_data = array(
                                "sys_userid" => $sys_userid,
                                "sys_groupid" => $sys_groupid,
                                "sys_perm_user" => 'riud',
                                "sys_perm_group" => 'riud',
                                "sys_perm_other" => '',
                                "server_id" => $server_id,
                                "email" => $user.'@'.$domain,
                                "login" => $login_name,
                                "password" => $password_enc,
                                "name" => $name,
                                "uid" => $uid,
                                "gid" => $gid,
                                "maildir" => $maildir,
                                "quota" => $quota_bytes,
				"cc" => '',
				"homedir" => $homedir,
                                "postfix" => 'y',
				"access" => 'n',
			);
			//die('INSERT INTO mail_user '.$insert_data);
			
			$app->db->datalogInsert('mail_user', $insert_data, 'mailuser_id');
		}
	}
	
	$app->tpl->setLoop('email_list',$email_list);
	$app->tpl->setVar('has_emails',1);
	
}

$app->tpl->setVar('msg',$msg);
$app->tpl->setVar('error',$error);

// Set the client list
$clients = $app->db->queryAllRecords("SELECT groupid,name FROM sys_group WHERE 1");
$clientlist = '';
foreach($clients as $c) {
	$clientlist .= "<option value='$c[groupid]'>$c[name]</option>";
}
$app->tpl->setVar('clientlist',$clientlist);

//* load language file
/*
$lng_file = 'lib/lang/'.$_SESSION['s']['language'].'_mailbox_import.lng';
include($lng_file);
$app->tpl->setVar($wb);
*/

$app->tpl_defaults();
$app->tpl->pparse();


?>
